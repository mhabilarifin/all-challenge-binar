package com.binar.Batch7.C4.Entity;

import com.binar.Batch7.C4.Entity.Merchant;
import lombok.Data;

import jakarta.persistence.*;

@Data
@Entity
@Table(name = "Product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long productId;

    @Column(name = "product_code")
    private String productCode;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "price")
    private double price;

    @ManyToOne
    @JoinColumn(name = "merchant_code", referencedColumnName = "merchant_code")
    private Merchant merchant;
}

