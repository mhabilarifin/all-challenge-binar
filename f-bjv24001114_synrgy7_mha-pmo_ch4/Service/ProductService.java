package com.binar.Batch7.Service;

import com.binar.Batch7.C4.Entity.Product;
import com.binar.Batch7.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    // Menambahkan produk baru
    public void addProduct(Product product) {
        productRepository.save(product);
    }

    // Mengupdate detail produk
    public void updateProduct(Product product) {
        productRepository.save(product);
    }

    // Menghapus produk
    public void deleteProduct(long productId) {
        productRepository.deleteById(productId);
    }

    // Menampilkan produk yang tersedia
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }
}

