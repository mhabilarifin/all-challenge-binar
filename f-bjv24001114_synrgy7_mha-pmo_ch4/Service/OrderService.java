package com.binar.Batch7.Service;

import com.binar.Batch7.C4.Entity.Order;
import com.binar.Batch7.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    // Membuat pesanan baru
    public void createOrder(Order order) {
        orderRepository.save(order);
    }

    // Menampilkan semua pesanan
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
}
