package com.binar.Batch7.Service;

import com.binar.Batch7.C4.Entity.Users;
import com.binar.Batch7.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    // Menambahkan user baru
    public void addUser(Users user) {
        userRepository.save(user);
    }

    // Mengupdate data user
    public void updateUser(Users user) {
        userRepository.save(user);
    }

    // Menghapus user
    public void deleteUser(long userId) {
        userRepository.deleteById(userId);
    }

    // Menampilkan semua user
    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }
}

