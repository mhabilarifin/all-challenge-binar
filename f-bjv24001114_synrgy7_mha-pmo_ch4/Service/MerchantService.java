package com.binar.Batch7.Service;

import com.binar.Batch7.C4.Entity.Merchant;
import com.binar.Batch7.Repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class MerchantService {

    @Autowired
    private MerchantRepository merchantRepository;

    // Menambahkan merchant baru
    public void addMerchant(Merchant merchant) {
        merchantRepository.save(merchant);
    }

    // Mengupdate status merchant buka/tutup


    // Menampilkan merchant yang sedang buka
//    public List<Merchant> getOpenMerchants() {
//        return merchantRepository.findByOpen(true);
//    }
}
