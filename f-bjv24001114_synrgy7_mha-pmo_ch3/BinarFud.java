import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

class Menu {
    private String nama;
    private double harga;

    public Menu(String nama, double harga) {
        this.nama = nama;
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public double getHarga() {
        return harga;
    }
}

class Pesanan {
    private Menu menu;
    private int jumlah;

    public Pesanan(Menu menu, int jumlah) {
        this.menu = menu;
        this.jumlah = jumlah;
    }

    public Menu getMenu() {
        return menu;
    }

    public int getJumlah() {
        return jumlah;
    }

    public double subtotal() {
        return menu.getHarga() * jumlah;
    }
}

public class BinarFud {
    private static final List<Menu> MENU_LIST = new ArrayList<>();

    static {
        MENU_LIST.add(new Menu("Nasi Goreng", 20000));
        MENU_LIST.add(new Menu("Mie Goreng", 15000));
        MENU_LIST.add(new Menu("Nasi + Ayam", 18000));
        MENU_LIST.add(new Menu("Es Teh", 5000));
        MENU_LIST.add(new Menu("Es Jeruk", 7000));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double totalHarga = 0;
        List<Pesanan> daftarPesanan = new ArrayList<>();

        System.out.println("Selamat datang di Binar Food!");
        System.out.println("Menu Makanan:");
        // Penggunaan lambda untuk mencetak setiap menu dari MENU_LIST
        MENU_LIST.forEach(menu -> System.out.println((MENU_LIST.indexOf(menu) + 1) + ". " + menu.getNama() + " - Rp " + menu.getHarga()));
        System.out.println("99. Pesan dan Bayar");
        System.out.println("0. Keluar");

        while (true) {
            System.out.print("Pilih nomor menu: ");
            int pilihan = scanner.nextInt();
            if (pilihan == 0) {
                break;
            } else if (pilihan == 99) {
                // Penggunaan Optional untuk mengecek apakah daftarPesanan kosong sebelum memproses pembayaran
                if (daftarPesanan.isEmpty()) {
                    System.out.println("Pesanan kosong. Tidak ada yang harus dibayar.");
                    continue;
                }
                break;
            } else if (pilihan < 0 || pilihan > MENU_LIST.size()) {
                System.out.println("Nomor menu tidak valid.");
                continue;
            }

            Menu menu = MENU_LIST.get(pilihan - 1);

            System.out.print("Masukkan jumlah pesanan: ");
            int jumlah = scanner.nextInt();
            if (jumlah <= 0) {
                System.out.println("Jumlah pesanan tidak valid.");
                continue;
            }

            daftarPesanan.add(new Pesanan(menu, jumlah));
        }

        if (!daftarPesanan.isEmpty()) {
            System.out.println("\nDaftar Pesanan:");
            System.out.println("=========================================");
            System.out.println("Menu\t\tHarga\tJumlah\tSubtotal");
            System.out.println("=========================================");
            // Penggunaan lambda untuk mencetak setiap pesanan dalam daftarPesanan
            daftarPesanan.forEach(pesanan -> System.out.println(pesanan.getMenu().getNama() + "\t\tRp " + pesanan.getMenu().getHarga() + "\t" + pesanan.getJumlah() + " pcs\t\tRp " + pesanan.subtotal()));

            // Penggunaan stream untuk menghitung total harga pesanan
            totalHarga = daftarPesanan.stream().mapToDouble(Pesanan::subtotal).sum();

            System.out.println("=========================================");
            System.out.println("Total Harga:\t\t\t\tRp " + totalHarga);
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("struk_pembelian.txt"))) {
            writer.write("Binar Food\n");
            writer.write("=========================================\n");
            writer.write("Menu\t\tHarga\tJumlah\tSubtotal\n");
            writer.write("=========================================\n");
            // Penggunaan lambda untuk menulis setiap pesanan dalam daftarPesanan ke file
            daftarPesanan.forEach(pesanan -> {
                try {
                    writer.write(pesanan.getMenu().getNama() + "\t\tRp " + pesanan.getMenu().getHarga() + "\t" + pesanan.getJumlah() + " pcs\t\tRp " + pesanan.subtotal() + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.write("=========================================\n");
            writer.write("Total Harga:\t\t\t\tRp " + totalHarga);
            System.out.println("Struk pembelian telah disimpan dalam file struk_pembelian.txt");
        } catch (IOException e) {
            System.err.println("Gagal menyimpan struk pembelian: " + e.getMessage());
        }
    }
}
